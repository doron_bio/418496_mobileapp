package th.ac.ku.android.sutee.dotdot.fragment;

import java.util.List;

import th.ac.ku.android.sutee.dotdot.fragment.DotView.DotDataSource;
import th.ac.ku.android.sutee.dotdot.fragment.DotView.OnDotTouchListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DotDrawFragment extends Fragment {

	Dots dots;
	DotView mDotView;

	private static final String TAG_DOTS = "TAG_DOTS";

	private static final int RADIUS_SIZE = 10;

	public static DotDrawFragment createInstance(Dots dots) {
		Bundle init = new Bundle();
		init.putParcelable(TAG_DOTS, dots);
		DotDrawFragment fragment = new DotDrawFragment();
		fragment.setArguments(init);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
		
		if (savedInstanceState == null) {
			savedInstanceState = getArguments();
		}

		if (savedInstanceState != null) {
			dots = savedInstanceState.getParcelable(TAG_DOTS);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.draw_fragment, container, false);
		mDotView = (DotView) view.findViewById(R.id.dotView);

		mDotView.setDataSource(new DotDataSource() {
			@Override
			public List<Dot> dots(DotView view) {
				return dots.getDots();
			}
		});

		mDotView.setOnDotTouchListener(new OnDotTouchListener() {
			@Override
			public void onDotTouch(int x, int y, float pressure, float size) {
				float radius = (pressure == 0 || size == 0) ? RADIUS_SIZE
						: RADIUS_SIZE * pressure * size;
				dots.addDot(x, y, Color.GRAY, (int)radius);
			}
		});

		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(TAG_DOTS, dots);
	}
	
	public void updateViews() {
		mDotView.invalidate();
	}

}
