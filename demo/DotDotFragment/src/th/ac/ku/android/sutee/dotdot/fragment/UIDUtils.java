package th.ac.ku.android.sutee.dotdot.fragment;

public class UIDUtils {
	private static long uid = 0;
	
	public static long getNextUID() {
        return ++uid;
    }
}
