package th.ac.ku.android.kubook;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;

public class KuBook {
	public static final class SimpleBooks implements BaseColumns {
		SimpleBooks() {
		}

		public static final String AUTHORITY = "th.ac.ku.android.kubook.SimpleBooks";
		public static final Uri BOOKS_URI = Uri.parse("content://" + AUTHORITY
				+ "/" + SimpleBooks.BOOKS);

		public static final Uri CONTENT_URI = BOOKS_URI;

		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.kubook.books";
		public static final String CONTENT_BOOK_TYPE = "vnd.android.cursor.item/vnd.kubook.books";

		public static final String BOOKS = "books";

	}

	@DatabaseTable(tableName = "books")
	public static final class Book {
		public static final String ID_COLUMN = "_id";
		public static final String TITLE_COLUMN = "title";
		public static final String PRICE_COLUMN = "price";
		public static final String AUTHOR_COLUMN = "author";
		public static final String ISBN_COLUMN = "isbn";

		@DatabaseField(generatedId = true, columnName = ID_COLUMN)
		private int id;

		@DatabaseField(canBeNull = false, columnName = TITLE_COLUMN, index = true)
		private String title;

		@DatabaseField(canBeNull = false, columnName = PRICE_COLUMN)
		private float price;

		@DatabaseField(canBeNull = false, columnName = AUTHOR_COLUMN, index = true)
		private String author;

		@DatabaseField(canBeNull = false, columnName = ISBN_COLUMN, index = true, unique = true)
		private String isbn;

		Book() {
		}

		public Book(String title, float price, String author, String isbn) {
			this.title = title;
			this.price = price;
			this.author = author;
			this.isbn = isbn;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public float getPrice() {
			return price;
		}

		public void setPrice(float price) {
			this.price = price;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getIsbn() {
			return isbn;
		}

		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}

		public int getId() {
			return id;
		}
	}

	public static final class DatabaseHelper extends OrmLiteSqliteOpenHelper {
		private static final String DATABASE_NAME = "kubook_content.db";
		private static final int DATABASE_VERSION = 1;

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database,
				ConnectionSource connectionSource) {
			try {
				TableUtils.createTable(connectionSource, KuBook.Book.class);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase database,
				ConnectionSource connectionSource, int oldVersion,
				int newVersion) {
			try {
				TableUtils.dropTable(connectionSource, KuBook.Book.class, true);
				onCreate(database);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		private RuntimeExceptionDao<KuBook.Book, Integer> mBookDao = null;

		public RuntimeExceptionDao<KuBook.Book, Integer> getBookDao()
				throws SQLException {
			if (mBookDao == null) {
				Dao<KuBook.Book, Integer> dao = getDao(KuBook.Book.class);
				mBookDao = new RuntimeExceptionDao<KuBook.Book, Integer>(dao);
			}
			return mBookDao;
		}
	}
}
