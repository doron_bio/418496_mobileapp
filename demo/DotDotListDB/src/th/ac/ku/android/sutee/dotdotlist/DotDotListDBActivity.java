package th.ac.ku.android.sutee.dotdotlist;

import java.sql.SQLException;
import java.util.Random;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.AndroidCompiledStatement;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder.StatementType;

public class DotDotListDBActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor> {

	private DotCursorAdapter mAdapter;
	Random mGenerator;
	private DatabaseHelper databaseHelper;

	public static final class DotCursorLoader extends SimpleCursorLoader {

		private DatabaseHelper mHelper;

		public DotCursorLoader(Context context, DatabaseHelper helper) {
			super(context);
			mHelper = helper;
		}

		@Override
		public Cursor loadInBackground() {
			Cursor cursor = null;
			QueryBuilder<Dot, Integer> qb = mHelper.getDotDao().queryBuilder();
			PreparedQuery<Dot> query;
			try {
				query = qb.prepare();
				AndroidCompiledStatement compiledStatement = (AndroidCompiledStatement) query
						.compile(mHelper.getConnectionSource()
								.getReadOnlyConnection(), StatementType.SELECT);
				cursor = compiledStatement.getCursor();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if (cursor != null) {
				cursor.getCount();
			}
			
			return cursor;
		}

	}

	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(this,
					DatabaseHelper.class);
		}
		return databaseHelper;
	}

	@Override
	protected void onDestroy() {
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
		super.onDestroy();
	}

	public final class DotCursorAdapter extends CursorAdapter {

		private Context mContext;

		public class ViewHolder {
			TextView coordXText;
			TextView coordYText;
		}

		public DotCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			mContext = context;
		}

		public DotCursorAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view = LayoutInflater.from(mContext).inflate(R.layout.row,
					parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.coordXText = (TextView) view
					.findViewById(R.id.coordXText);
			viewHolder.coordYText = (TextView) view
					.findViewById(R.id.coordYText);
			view.setTag(viewHolder);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			ViewHolder viewHolder = (ViewHolder) view.getTag();
			int x = cursor.getInt(cursor.getColumnIndex(Dot.X_COLUME));
			int y = cursor.getInt(cursor.getColumnIndex(Dot.Y_COLUME));
			viewHolder.coordXText.setText(String.valueOf(x));
			viewHolder.coordYText.setText(String.valueOf(y));
		}

	}

	public void randomDots(View view) {
		int x = mGenerator.nextInt(250);
		int y = mGenerator.nextInt(250);

		Dot dot = new Dot(x, y);
		try {
			getHelper().getDotDao().create(dot);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Loader<Cursor> loader = getSupportLoaderManager().getLoader(0);
		if (loader != null && loader.isStarted()) {
			loader.forceLoad();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mAdapter = new DotCursorAdapter(this, null, true);

		mGenerator = new Random();
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(mAdapter);

		getSupportLoaderManager().initLoader(0, null, this);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long id) {
				Cursor cursor = mAdapter.getCursor();
				cursor.moveToPosition(position);
				Toast.makeText(
						DotDotListDBActivity.this,
						cursor.getInt(cursor.getColumnIndex(Dot.X_COLUME))
								+ ":"
								+ cursor.getInt(cursor
										.getColumnIndex(Dot.Y_COLUME)),
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new DotCursorLoader(this, getHelper());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
}