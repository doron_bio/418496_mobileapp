//
//  Dot.m
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Dot.h"

@implementation Dot

@synthesize origin = _origin;
@synthesize size = _size;

- (id)initWithOrigin:(CGPoint)origin
{
    self = [super init];
    
    if (self) {
        self.origin = origin;
    }
    
    return self;
}

@end
