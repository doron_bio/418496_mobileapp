//
//  Dot.h
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dot : NSObject

@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) NSUInteger size;

- (id)initWithOrigin:(CGPoint)origin;

@end
