//
//  DotDotViewController.h
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dots.h"
#import "DotView.h"

@interface DotDotViewController : UIViewController<DotViewDataSource, DotsDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet DotView *dotView;
@property (nonatomic, strong) UIColor *dotColor;


@end
