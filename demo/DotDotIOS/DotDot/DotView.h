//
//  DotView.h
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DotViewDataSource <NSObject>

- (NSArray *)dotItems;

@end

@interface DotView : UIView

@property (nonatomic, weak) IBOutlet id<DotViewDataSource> dataSource;
@property (nonatomic, strong) UIColor *dotColor;

@end
