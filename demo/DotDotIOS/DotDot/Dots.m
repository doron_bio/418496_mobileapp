//
//  Dots.m
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Dots.h"
#import "Dot.h"

@interface Dots()

@property (nonatomic, strong) NSMutableArray *dots;

@end

@implementation Dots

@synthesize dots = _dots;
@synthesize delegate = _delegate;
@synthesize dotItems = _dotItems;

- (NSMutableArray *)dots
{
    if (_dots == nil) {
        _dots = [[NSMutableArray alloc] init];
    }
    return _dots;
}

- (void)addDot:(Dot *)dot
{
    [self.dots addObject:dot];
    [self.delegate dotsDidChange:[self.dots copy]];
}

- (NSArray *)dotItems
{
    return [self.dots copy];
}

@end
