//
//  DotTableViewController.h
//  DotDot
//
//  Created by Sutee Sudprasert on 3/5/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dots.h"

@interface DotTableViewController : UITableViewController

@property (nonatomic, strong) Dots *model;

@end
