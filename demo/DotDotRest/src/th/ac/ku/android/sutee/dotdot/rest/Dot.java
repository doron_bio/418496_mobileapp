package th.ac.ku.android.sutee.dotdot.rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "dot")
public class Dot {

	public static final String ID_COLUMN = "_id";
	public static final String X_COLUMN = "x";
	public static final String Y_COLUMN = "y";
	public static final String GID_COLUMN = "gid";
	public static final String STATE_POSTING_COLUMN = "state_posting";
	public static final String STATE_UPDATING_COLUMN = "state_updating";
	public static final String STATE_DELETING_COLUMN = "state_deleting";

	@DatabaseField(generatedId = true, columnName = ID_COLUMN)
	private int id;

	@DatabaseField(unique = true, columnName = GID_COLUMN)
	private String gid;

	@DatabaseField(columnName = X_COLUMN)
	private int x;

	@DatabaseField(columnName = Y_COLUMN)
	private int y;

	@DatabaseField(columnName = STATE_POSTING_COLUMN)
	boolean statePosting;

	@DatabaseField(columnName = STATE_UPDATING_COLUMN)
	boolean stateUpdating;

	@DatabaseField(columnName = STATE_DELETING_COLUMN)
	boolean stateDeleting;

	Dot() {
	}

	public Dot(int x, int y) {
		this.x = x;
		this.y = y;
		this.statePosting = false;
		this.stateUpdating = false;
		this.stateDeleting = false;
	}

	public Dot(int x, int y, String gid) {
		this(x, y);
		this.gid = gid;
	}

	public Dot(int x, int y, String gid, boolean statePosting,
			boolean stateUpdating, boolean stateDeleting) {
		this(x, y, gid);
		this.statePosting = statePosting;
		this.stateUpdating = stateUpdating;
		this.stateDeleting = stateDeleting;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getId() {
		return id;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public boolean isStatePosting() {
		return statePosting;
	}

	public void setStatePosting(boolean statePosting) {
		this.statePosting = statePosting;
	}

	public boolean isStateUpdating() {
		return stateUpdating;
	}

	public void setStateUpdating(boolean stateUpdating) {
		this.stateUpdating = stateUpdating;
	}

	public boolean isStateDeleting() {
		return stateDeleting;
	}

	public void setStateDeleting(boolean stateDeleting) {
		this.stateDeleting = stateDeleting;
	}
}
