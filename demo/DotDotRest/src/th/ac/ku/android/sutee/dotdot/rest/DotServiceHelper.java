package th.ac.ku.android.sutee.dotdot.rest;

import java.util.ArrayList;
import java.util.HashMap;

import th.ac.ku.android.sutee.dotdot.rest.RestClient.RequestMethod;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

public class DotServiceHelper {

	private static HashMap<String, Intent> requestMap = new HashMap<String, Intent>(
			10);
	private static ArrayList<String> mRequests = new ArrayList<String>(10);

	public static void performPendingOperations(Context context) {
		Cursor cursor = context.getContentResolver().query(
				DotProvider.CONTENT_URI, null, null, null, null);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			int x = cursor.getInt(cursor.getColumnIndex(Dot.X_COLUMN));
			int y = cursor.getInt(cursor.getColumnIndex(Dot.Y_COLUMN));
			String gid = cursor
					.getString(cursor.getColumnIndex(Dot.GID_COLUMN));
			boolean statePosting = (cursor.getInt(cursor
					.getColumnIndex(Dot.STATE_POSTING_COLUMN)) == 1) ? true
					: false;
			boolean stateUpdating = (cursor.getInt(cursor
					.getColumnIndex(Dot.STATE_UPDATING_COLUMN)) == 1) ? true
					: false;
			boolean stateDeleting = (cursor.getInt(cursor
					.getColumnIndex(Dot.STATE_DELETING_COLUMN)) == 1) ? true
					: false;

			if (statePosting) {
				Dot dot = new Dot(x, y, gid);
				DotServiceHelper.performPostOperation(context, gid, dot, false);
			}

			if (stateUpdating) {
				// TODO: updating
			}

			if (stateDeleting) {
				// TODO: deleting
			}
		}
		cursor.close();
	}

	public static void performGetOperation(Context context, String requestId) {
		Intent intent = new Intent(context, DotService.class);
		intent.putExtra(DotService.METHOD_KEY, RequestMethod.GET.toString());
		intent.putExtra(DotService.REQUEST_ID_KEY, requestId);

		addRequest(context, requestId, intent, RequestMethod.GET);
	}

	public static void performPostOperation(Context context, String requestId,
			Dot dot, boolean localPost) {
		Intent intent = new Intent(context, DotService.class);
		intent.putExtra(DotService.METHOD_KEY, RequestMethod.POST.toString());
		intent.putExtra(DotService.REQUEST_ID_KEY, requestId);
		intent.putExtra(DotService.X_KEY, dot.getX());
		intent.putExtra(DotService.Y_KEY, dot.getY());
		intent.putExtra(DotService.GID_KEY, dot.getGid());

		addRequest(context, requestId, intent, RequestMethod.POST);
		if (localPost) {
			doLocalPost(context, intent);
		}
	}

	private static void doLocalPost(Context context, Intent intent) {
		int x = intent.getIntExtra(DotService.X_KEY, 0);
		int y = intent.getIntExtra(DotService.Y_KEY, 0);
		String gid = intent.getStringExtra(DotService.GID_KEY);

		ContentValues values = new ContentValues();
		values.put(Dot.X_COLUMN, x);
		values.put(Dot.Y_COLUMN, y);
		values.put(Dot.GID_COLUMN, gid);
		values.put(Dot.STATE_POSTING_COLUMN, true);

		context.getContentResolver().insert(DotProvider.CONTENT_URI, values);
	}

	private static void addRequest(Context context, String requestId,
			Intent intent, RequestMethod method) {
		requestMap.put(requestId, intent);
		mRequests.add(requestId);

		if (requestMap.size() == 1) {
			context.startService(intent);
		}
	}

	public static void removeRequest(String requestId) {
		requestMap.remove(requestId);
		mRequests.remove(requestId);
	}

	public static void processRequests(Context context) {
		if (requestMap.size() > 0) {
			String requestId = mRequests.get(0);
			Intent intent = requestMap.get(requestId);
			context.startService(intent);
		}
	}

	public static int getRequestSize() {
		return mRequests.size();
	}

}
