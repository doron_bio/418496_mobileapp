package th.ac.ku.android.sutee.dotdot.rest;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import th.ac.ku.android.sutee.dotdot.rest.RestClient.OnRequestFinishListener;
import th.ac.ku.android.sutee.dotdot.rest.RestClient.RequestMethod;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class DotService extends Service implements OnRequestFinishListener {

	private static final String TAG = "DotService";

	public static final String METHOD_KEY = "METHOD_KEY";
	public static final String REQUEST_ID_KEY = "REQUEST_ID_KEY";
	public static final String X_KEY = "X_KEY";
	public static final String Y_KEY = "Y_KEY";
	public static final String GID_KEY = "GID_KEY";
	public static final String STATE_POSTING_KEY = "STATE_POSTING_KEY";

	private static final String REST_URL = "http://10.0.2.2:8000/dots/";
	private String currentRequestId;

	private DotServiceBinder mBinder;

	public class DotServiceBinder extends Binder {
		DotService getService() {
			return DotService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		if (mBinder == null) {
			mBinder = new DotServiceBinder();
		}
		return mBinder;
	}

	private DotServiceCallbackListener callbackListener;

	public static interface DotServiceCallbackListener {
		void onOperationComplete(String requestId);

		void onOperationFail(String requestId);
	}

	public void setCallbackListener(DotServiceCallbackListener callbackListener) {
		this.callbackListener = callbackListener;
	}

	private void notifyOperationComplete(String requestId) {
		if (this.callbackListener != null) {
			this.callbackListener.onOperationComplete(requestId);
		}
	}

	private void notifyOperationFail(String requestId) {
		if (this.callbackListener != null) {
			this.callbackListener.onOperationFail(requestId);
		}
	}

	public String getCurrentRequestId() {
		return currentRequestId;
	}

	public void setCurrentRequestId(String currentRequestId) {
		this.currentRequestId = currentRequestId;
	}

	private void doGet() {
		RestClient restClient = new RestClient(REST_URL);
		restClient.setOnRequestFinishListener(this);
		restClient.execute(RequestMethod.GET);
	}

	private void doPost(Intent intent) {
		int x = intent.getIntExtra(DotService.X_KEY, 0);
		int y = intent.getIntExtra(DotService.Y_KEY, 0);
		String gid = intent.getStringExtra(DotService.GID_KEY);
		int radius = 0;
		int color = 0;
		int alpha = 1;

		RestClient restClient = new RestClient(REST_URL);
		restClient.addParam("x", x + "");
		restClient.addParam("y", y + "");
		restClient.addParam("gid", gid);
		restClient.addParam("radius", radius + "");
		restClient.addParam("color", color + "");
		restClient.addParam("alpha", alpha + "");
		restClient.setOnRequestFinishListener(this);
		restClient.execute(RequestMethod.POST);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		String method = intent.getStringExtra(METHOD_KEY);
		setCurrentRequestId(intent.getStringExtra(REQUEST_ID_KEY));

		switch (RequestMethod.valueOf(method)) {
		case GET:
			doGet();
			break;
		case POST:
			doPost(intent);
			break;
		case PUT:
			break;
		case DELETE:
			break;
		default:
			throw new IllegalArgumentException("Unsupported Method: " + method);
		}

		return START_NOT_STICKY;
	}

	private void processGetResponse(String response) {
		new JSONResponseHandler() {
			@Override
			public void onProcess(JSONArray jsonArray) {
				ArrayList<String> gids = new ArrayList<String>(
						jsonArray.length());
				for (int index = 0; index < jsonArray.length(); index++) {
					JSONObject jsObject;
					int x, y;
					String gid;

					// skip the record that can't be parsed
					try {
						jsObject = jsonArray.getJSONObject(index);
						gid = jsObject.getString("gid");
						x = jsObject.getInt("x");
						y = jsObject.getInt("y");
					} catch (JSONException e) {
						e.printStackTrace();
						continue;
					}

					Cursor cursor = getContentResolver().query(
							DotProvider.CONTENT_URI, null, "gid = ?",
							new String[] { String.valueOf(gid) }, null);

					ContentValues values = new ContentValues();
					values.put(Dot.X_COLUMN, x);
					values.put(Dot.Y_COLUMN, y);
					values.put(Dot.GID_COLUMN, gid);

					if (cursor.getCount() == 0) {
						// this item is inserted remotely
						getContentResolver().insert(DotProvider.CONTENT_URI,
								values);
					} else {
						int isPosting = cursor.getInt(cursor
								.getColumnIndex(Dot.STATE_POSTING_COLUMN));
						int isUpdating = cursor.getInt(cursor
								.getColumnIndex(Dot.STATE_UPDATING_COLUMN));
						int isDeleting = cursor.getInt(cursor
								.getColumnIndex(Dot.STATE_DELETING_COLUMN));

						// skip the record that is pending
						if (isPosting + isUpdating + isDeleting == 0) {
							String localKey = String.format("%s:%d:%d", cursor
									.getString(cursor
											.getColumnIndex(Dot.GID_COLUMN)),
									cursor.getInt(cursor
											.getColumnIndex(Dot.X_COLUMN)),
									cursor.getInt(cursor
											.getColumnIndex(Dot.Y_COLUMN)));
							String remoteKey = String.format("%s:%d:%d", gid,
									x, y);

							if (!localKey.equals(remoteKey)) {
								// this item is updated remotely
								getContentResolver().update(
										DotProvider.CONTENT_URI, values,
										"gid = ?", new String[] { gid });
							}
						}
					}
					cursor.close();
					// collect GID for detecting deletion in server side
					gids.add(gid);
				}

				Cursor cursor = getContentResolver().query(
						DotProvider.CONTENT_URI,
						new String[] { Dot.GID_COLUMN }, null, null, null);
				for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor
						.moveToNext()) {
					String gid = cursor.getString(cursor
							.getColumnIndex(Dot.GID_COLUMN));
					if (!gids.contains(gid)) {
						// this item is deleted remotely
						getContentResolver().delete(DotProvider.CONTENT_URI,
								"gid = ?", new String[] { gid });
					}
				}
				cursor.close();
			}
		}.process(response);
	}

	void clearStatePosting(String requestId) {
		ContentValues values = new ContentValues();
		values.put(Dot.STATE_POSTING_COLUMN, false);
		getContentResolver().update(DotProvider.CONTENT_URI, values, "gid = ?",
				new String[] { requestId });
	}

	@Override
	public void onRequestFinish(RequestMethod method, int responseCode,
			String message, String response) {
		String requestId = getCurrentRequestId();
		Log.d(TAG, "requestId = " + requestId + "");
		Log.d(TAG, "responseCode = " + responseCode + "");

		switch (method) {
		case GET:
			processGetResponse(response);
			break;
		case POST:
			clearStatePosting(requestId);
			break;
		case PUT:
			break;
		case DELETE:
			break;
		default:
			throw new IllegalArgumentException("Unsupported Method: " + method);
		}

		DotServiceHelper.removeRequest(requestId);
		DotServiceHelper.processRequests(getBaseContext());
		this.notifyOperationComplete(requestId);
	}

	@Override
	public void onRequestFinishWithError() {
		String requestId = getCurrentRequestId();
		DotServiceHelper.removeRequest(requestId);
		DotServiceHelper.processRequests(getBaseContext());
		this.notifyOperationFail(requestId);
	}

}
