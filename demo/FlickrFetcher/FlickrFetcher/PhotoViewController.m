//
//  PhotoViewController.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PhotoViewController.h"
#import "ASIHTTPRequest.h"
#import "MBProgressHUD.h"

@interface PhotoViewController ()<ASIHTTPRequestDelegate, ASIProgressDelegate, MBProgressHUDDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) ASIHTTPRequest *request;
@property (nonatomic, strong) MBProgressHUD *progressHUD;

@end

@implementation PhotoViewController
@synthesize imageView = _imageView;
@synthesize request = _request;
@synthesize url = _url;
@synthesize myToolbar = _myToolbar;
@synthesize scrollView = _scrollView;
@synthesize progressHUD = _progressHUD;
@synthesize splitViewBarButtonItem = _splitViewBarButtonItem;


- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    if (_splitViewBarButtonItem != splitViewBarButtonItem) {
        _splitViewBarButtonItem = splitViewBarButtonItem;            
        NSMutableArray *items = [NSMutableArray arrayWithArray:self.myToolbar.items];
        if (splitViewBarButtonItem) {
            [items insertObject:splitViewBarButtonItem atIndex:0];
        } else {
            [items removeObjectAtIndex:0];
        }        
        [self.myToolbar setItems:items animated:YES];
    }
}

- (void)setUrl:(NSURL *)url
{
    if (_url != url) {
        _url = url;
        if (self.splitViewController) {
            [self loadImage:url];
        }
    }
}

- (void)cancelDownload:(UIGestureRecognizer *)recognizer
{
    [self.request clearDelegatesAndCancel];
    [self.progressHUD hide:YES];
}

- (MBProgressHUD *)progressHUD
{
    if (_progressHUD == nil) {
        _progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
        _progressHUD.delegate = self;
        _progressHUD.mode = MBProgressHUDModeDeterminate;
        _progressHUD.dimBackground = YES;
        _progressHUD.labelText = @"Loading";
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelDownload:)];
        [_progressHUD addGestureRecognizer:tapGestureRecognizer];
    }
    return _progressHUD;
}

- (void)imageRequestDidFinish:(ASIHTTPRequest *)request
{
    [self.progressHUD hide:YES];    
    self.imageView.image = [UIImage imageWithData:request.responseData];
        
    // fading animation
    self.imageView.alpha = 0.0;
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationCurveEaseOut 
                     animations:^{
                         self.imageView.alpha = 1.0;
                     } 
                     completion:^(BOOL finished) {
                         // do somethings when animation finished
                     }];
}

- (void)imageRequestDidFail:(ASIHTTPRequest *)request
{
    [self.progressHUD hide:YES];    
}

- (void)imageRequestDidStart:(ASIHTTPRequest *)request
{
    [self.view.window addSubview:self.progressHUD];
    self.progressHUD.progress = 0.0;
    [self.progressHUD show:YES];
}

- (void)setProgress:(float)newProgress
{
    self.progressHUD.progress = newProgress;
    self.progressHUD.labelText = [NSString stringWithFormat:@"%.0f%%", 100 * newProgress];
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [self.progressHUD removeFromSuperview];
    self.progressHUD = nil;
}

- (void)loadImage:(NSURL *)url
{
    self.request = [ASIHTTPRequest requestWithURL:url];
    self.request.delegate = self;
    self.request.downloadProgressDelegate = self;
    self.request.allowResumeForFileDownloads = YES;
    
    [self.request setDidFinishSelector:@selector(imageRequestDidFinish:)];
    [self.request setDidFailSelector:@selector(imageRequestDidFail:)];
    [self.request setDidStartSelector:@selector(imageRequestDidStart:)];
    
    [self.request startAsynchronous];    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.splitViewController == nil) {
        [self loadImage:self.url];
    }    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    // very important!!! always cancel the request and clear its delegate in viewDid/WillDisapper or viewDidUnload or dealloc (choose one that is most suitable for your application)
    [self.request clearDelegatesAndCancel];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Photo";
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale = 0.5;
    self.scrollView.maximumZoomScale = 6.0;
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [self setMyToolbar:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

@end
