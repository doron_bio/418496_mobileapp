//
//  DetailViewController.h
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SplitViewBarButtonItemPresenter <NSObject>

@property (nonatomic, strong) UIBarButtonItem *splitViewBarButtonItem;

@end

@interface DetailViewController : UIViewController

@property (nonatomic, weak) UIPopoverController *popoverController;

@end
