//
//  PhotoTableViewController.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PhotoTableViewController.h"
#import "PhotoViewController.h"
#import "Photo+Flickr.h"
#import "Photographer.h"

#import "JSONKit.h"

@interface PhotoTableViewController ()

@property (nonatomic, strong) NSArray *photos;

@property (nonatomic, strong) UIBarButtonItem *refreshBarButtonItem;

- (void)useDatabase;
- (void)fetchFlickrPhotosIntoDocument:(UIManagedDocument *)document;

@end

@implementation PhotoTableViewController
@synthesize photoDatabase = _photoDatabase;
@synthesize refreshBarButtonItem = _refreshBarButtonItem;

@synthesize photos = _photos;

- (void)setPhotoDatabase:(UIManagedDocument *)photoDatabase
{
    if (_photoDatabase != photoDatabase) {
        _photoDatabase = photoDatabase;
        [self useDatabase];
    }
}

- (void)setPhotos:(NSArray *)photos
{
    if (_photos != photos) {
        _photos = photos;
        [self.tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Photo"]) {
        PhotoViewController *photoViewController = (PhotoViewController *)segue.destinationViewController;
        Photo *photo = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForCell:sender]];        
        photoViewController.url = [NSURL URLWithString:photo.url];        
    }
}

- (IBAction)refresh:(id)sender
{
    self.refreshBarButtonItem = sender;
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner startAnimating];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
    self.navigationItem.rightBarButtonItem = buttonItem;
    
    [self fetchFlickrPhotosIntoDocument:self.photoDatabase];
}

- (void)fetchFlickrPhotosIntoDocument:(UIManagedDocument *)document
{
    NSString *urlString = @"http://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=c31eef9fe165cff845d468d76d4a8aa9&format=json";    

    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSError *error = nil;        
        NSString *responseString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSUTF8StringEncoding error:&error];
        NSString *jsonString = [[responseString stringByReplacingCharactersInRange:NSMakeRange(0, 13) withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];        

        // put photo into database
        [self.photoDatabase.managedObjectContext performBlock:^{
            for (id photoInfo in [[[jsonString objectFromJSONString] objectForKey:@"photos"] objectForKey:@"photo"]) {
                [Photo photoWithFlickrInfo:photoInfo inManagedObjectContext:self.photoDatabase.managedObjectContext];
            }            
        }]; 
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.navigationItem.rightBarButtonItem = self.refreshBarButtonItem;
        });        
    });
}

- (void)setupFetchResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO]];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.photoDatabase.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController = fetchedResultsController;
}

- (void)useDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.photoDatabase.fileURL.path]) {
        [self.photoDatabase saveToURL:self.photoDatabase.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            [self setupFetchResultsController];
            [self fetchFlickrPhotosIntoDocument:self.photoDatabase];
        }];
    } else if (self.photoDatabase.documentState == UIDocumentStateClosed) {
        [self.photoDatabase openWithCompletionHandler:^(BOOL success) {
            [self setupFetchResultsController];           
        }];
    } else if (self.photoDatabase.documentState == UIDocumentStateNormal) {
        [self setupFetchResultsController];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.photoDatabase == nil) {
        self.photoDatabase = [[UIManagedDocument alloc] initWithFileURL:[NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:@"Default Database"]]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    PhotoViewController *photoViewController = self.splitViewController.viewControllers.lastObject; 
    NSLog(@"%@", photoViewController.popoverController);
    [photoViewController.popoverController dismissPopoverAnimated:YES];        
    photoViewController.url = [NSURL URLWithString:photo.url];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Photo Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.owner.uid;
    
    return cell;
}


@end
