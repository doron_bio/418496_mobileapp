//
//  Photographer+Create.h
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photographer.h"

@interface Photographer (Create)

+ (Photographer *)photographerWithUID:(NSString *)uid inManagedObjectContext:(NSManagedObjectContext *)context;

@end
