//
//  DetailViewController.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController () <UISplitViewControllerDelegate>

@end

@implementation DetailViewController

@synthesize popoverController;

- (id<SplitViewBarButtonItemPresenter>)splitViewBarButtonItemPresenter
{
    if ([self.splitViewController.viewControllers.lastObject conformsToProtocol:@protocol(SplitViewBarButtonItemPresenter)]) {
        return self.splitViewController.viewControllers.lastObject;
    }
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    self.splitViewController.delegate = self;
}

- (void)splitViewController:(UISplitViewController *)svc popoverController:(UIPopoverController *)pc willPresentViewController:(UIViewController *)aViewController
{
    self.popoverController = pc;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return [self splitViewBarButtonItemPresenter] ? UIInterfaceOrientationIsPortrait(orientation): NO;
}

- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = self.title;
    [self splitViewBarButtonItemPresenter].splitViewBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self splitViewBarButtonItemPresenter].splitViewBarButtonItem = nil;    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
