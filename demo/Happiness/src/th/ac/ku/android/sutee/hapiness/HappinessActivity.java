package th.ac.ku.android.sutee.hapiness;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.TextView;

public class HappinessActivity extends Activity implements FaceView.FaceViewDataSource {
	private int happiness;

	private FaceView faceView;
	private TextView happinessText;

	public TextView getHappinessText() {
		return (happinessText == null) ? (happinessText = (TextView) findViewById(R.id.happinessText))
				: happinessText;
	}

	public int getHappiness() {
		return happiness;
	}

	public void setHappiness(int happiness) {
		if (this.happiness != happiness) {
			happiness = (happiness < 0) ? 0 : happiness;
			happiness = (happiness > 100) ? 100 : happiness;
			this.happiness = happiness;
//			getFaceView().setHappiness(this.happiness);
			getHappinessText().setText("Happiness is " + this.happiness);
			getFaceView().invalidate();
		}
	}

	public FaceView getFaceView() {
		return (faceView == null) ? (faceView = (FaceView) findViewById(R.id.faceView))
				: faceView;
	}

	private GestureDetector gestureDetector;
	private ScaleGestureDetector scaleGestureDetector;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setHappiness(50);
		getFaceView().setDataSource(this);
		gestureDetector = new GestureDetector(this,
				new SimpleOnGestureListener() {
					@Override
					public boolean onScroll(MotionEvent e1, MotionEvent e2,
							float distanceX, float distanceY) {
						setHappiness(getHappiness() + (int) distanceY);
						return true;
					}
				});

		scaleGestureDetector = new ScaleGestureDetector(this, getFaceView());

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return scaleGestureDetector.onTouchEvent(event)
				&& gestureDetector.onTouchEvent(event);
	}

	@Override
	public float smilingFactor(FaceView faceView) {
		return -1 + 0.02f * getHappiness();
	}

}