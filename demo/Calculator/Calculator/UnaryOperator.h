//
//  UnaryOperator.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Operator.h"
#import "Operand.h"

@interface UnaryOperator : Operator

- (double)compute:(Operand *)operand;
- (id)init;

@end
