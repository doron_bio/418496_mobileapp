//
//  LeftParenthesisToken.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Token.h"

@interface LeftParenthesisToken : Token

- (id)init;
- (NSString *)description;

@end
