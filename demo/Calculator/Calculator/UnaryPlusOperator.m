//
//  UnaryPlusOperator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "UnaryPlusOperator.h"

@implementation UnaryPlusOperator

- (double)compute:(Operand *)operand
{
    return operand.value;
}

@end
