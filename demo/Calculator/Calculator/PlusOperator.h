//
//  BinaryPlusOperator.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "BinaryOperator.h"

@interface PlusOperator : BinaryOperator

- (NSString *)description;

@end
