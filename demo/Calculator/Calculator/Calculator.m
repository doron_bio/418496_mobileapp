//
//  Calculator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Calculator.h"
#import "Token.h"
#import "Operand.h"
#import "Operator.h"
#import "BinaryOperator.h"
#import "UnaryOperator.h"

@interface Calculator ()

@property (nonatomic, strong) NSMutableArray *stack;

@end

@implementation Calculator

@synthesize stack = _stack;
@synthesize delegate = _delegate;

- (NSMutableArray *)stack
{
    if (_stack == nil) {
        _stack = [[NSMutableArray alloc] init];
    }
    return _stack;
}

- (void)pushToken:(Token *)token
{
    [self.stack addObject:token];
    [self.delegate calculator:self didFinishUpdateStack:[self.stack copy]];
}

- (void)evaluate
{
    NSMutableArray *postfixStack = [[NSMutableArray alloc] initWithCapacity:self.stack.count];
    NSMutableArray *operatorStack = [[NSMutableArray alloc] initWithCapacity:self.stack.count];
    NSMutableArray *backupStack = [self.stack mutableCopy];
    
    while (self.stack.count > 0) {
        Token *token = [self.stack objectAtIndex:0];
        NSLog(@"%@", token);
        if (token.type == TokenTypeOperand) {
            [postfixStack addObject:token];
        } else if (token.type == TokenTypeLeftParenthesis) {
            [operatorStack addObject:token];
        } else if (token.type == TokenTypeRightParenthesis) {
            while (((Token *)operatorStack.lastObject).type != TokenTypeLeftParenthesis) {
                [postfixStack addObject:operatorStack.lastObject];
                [operatorStack removeLastObject];
            }
            [operatorStack removeLastObject];
        } else if (token.type == TokenTypeOperator) {
            Operator *operator = (Operator *)token;            
            while (operatorStack.count > 0 && ((Token *)operatorStack.lastObject).type != TokenTypeLeftParenthesis &&
                   (operator.precedence > ((Operator *)operatorStack.lastObject).precedence 
                    || (operator.precedence == ((Operator *)operatorStack.lastObject).precedence && operator.associativity == OperatorAssociativityLeftToRight))) {                
                [postfixStack addObject:operatorStack.lastObject];
                [operatorStack removeLastObject];
            }
            [operatorStack addObject:operator];
        } else {
            NSLog(@"program should not reach here");
        }        
        [self.stack removeObjectAtIndex:0];
    }
    
    while (operatorStack.count > 0) {
        [postfixStack addObject:operatorStack.lastObject];
        [operatorStack removeLastObject];
    }
    
    NSMutableArray *tmpStack = [[NSMutableArray alloc] initWithCapacity:postfixStack.count];
    while (postfixStack.count > 0) {
        Token *token = [postfixStack objectAtIndex:0];
        if (token.type == TokenTypeOperand) {
            [tmpStack addObject:token];
        } else if (token.type == TokenTypeOperator) {
            Operator *operator = (Operator *)token;
            double result = 0;
            if (operator.operatorType == OperatorTypeUnary && tmpStack.count > 0) {
                Operand *operand = tmpStack.lastObject;
                [tmpStack removeLastObject];                
                result = [((UnaryOperator *)operator) compute:operand];
            } else if (operator.operatorType == OperatorTypeBinary && tmpStack.count > 1) {
                Operand *rightOperand = tmpStack.lastObject;
                [tmpStack removeLastObject];
                Operand *leftOperand = tmpStack.lastObject;
                [tmpStack removeLastObject];
                result = [((BinaryOperator *)operator) computeWithLeftOperand:leftOperand andRightOperand:rightOperand];
            } else {
                NSLog(@"program should not reach here");
                break;
            }
            [tmpStack addObject:[[Operand alloc] initWithValue:result]];
        } else {
            NSLog(@"program should not reach here");
            break;
        }
        [postfixStack removeObjectAtIndex:0];
    }
    
    if (postfixStack.count == 0 && tmpStack.count == 1) {
        [self.delegate calculator:self didFinishComputeWithResult:((Operand *)tmpStack.lastObject).value];
        self.stack = tmpStack;
        [self.delegate calculator:self didFinishUpdateStack:[self.stack copy]];
    } else {
        [self.delegate calculator:self didFinishComputeWithError:nil];
        self.stack = backupStack;
    }
    
}

@end
