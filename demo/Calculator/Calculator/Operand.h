//
//  Operand.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Token.h"

@interface Operand : Token

@property (nonatomic, assign) double value;

- (id)init;
- (id)initWithValue:(double) value;
- (NSString *)description;

@end
