//
//  Operand.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Operand.h"

@implementation Operand

@synthesize value = _value;

- (id)init
{
    self = [super init];
    if (self) {
        self.type = TokenTypeOperand;
    }
    return self;
}

- (id)initWithValue:(double) value
{
    self = [self init];
    
    if (self) {
        self.value = value;
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%.2lf", self.value];
}

@end
