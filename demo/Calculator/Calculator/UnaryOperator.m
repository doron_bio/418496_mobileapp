//
//  UnaryOperator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "UnaryOperator.h"

@implementation UnaryOperator

- (id)init
{
    self = [super init];
    if (self) {
        self.operatorType = OperatorTypeUnary;
    }
    return self;
}

- (double)compute:(Operand *)operand
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException 
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] 
                                 userInfo:nil];
}

@end
