package th.ac.ku.android.sutee.dotdotlist;

import java.util.Random;

import th.ac.ku.android.sutee.dotdotlist.Dots.OnDotsChangeListener;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DotDotListActivity extends Activity {

	private Dots mDots;
	private DotsAdapter mAdapter;
	Random mGenerator;

	public final class DotsAdapter extends BaseAdapter implements ListAdapter {

		LayoutInflater mInflater;

		public DotsAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public class ViewHolder {
			TextView coordXText;
			TextView coordYText;
		}

		@Override
		public int getCount() {
			return mDots.size();
		}

		@Override
		public Object getItem(int position) {
			return mDots.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.row, parent, false);
				holder.coordXText = (TextView) convertView
						.findViewById(R.id.coordXText);
				holder.coordYText = (TextView) convertView
						.findViewById(R.id.coordYText);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.coordXText.setText(mDots.get(position).getX() + "");
			holder.coordYText.setText(mDots.get(position).getY() + "");

			return convertView;
		}
	}

	public void randomDots(View view) {
		int x = mGenerator.nextInt(250);
		int y = mGenerator.nextInt(250);
		Dots.Dot dot = new Dots.Dot(x, y);
		mDots.add(dot);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mDots = new Dots();
		mAdapter = new DotsAdapter(this);
		mGenerator = new Random();
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(mAdapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long id) {
				Dots.Dot dot = mDots.get(position);
				// call another activity for editing the dot
				// and don't forget to declare the another activity in the
				// manifest file
				Toast.makeText(DotDotListActivity.this,
						dot.getX() + ":" + dot.getY(), Toast.LENGTH_SHORT)
						.show();
			}
		});

		mDots.setOnDotsChangeListener(new OnDotsChangeListener() {
			@Override
			public void onDotsChange() {
				mAdapter.notifyDataSetChanged();
			}
		});

	}
}